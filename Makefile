# Target specific macros
TARGET = nekonote15
NXTOSEK_ROOT = /usr/local/nxtOSEK
TARGET_SOURCES = \
	nekonote15.c
TOPPERS_OSEK_OIL_SOURCE = ./nekonote15.oil

# Don't modify below part
O_PATH ?= build
include ../../ecrobot/ecrobot.mak
