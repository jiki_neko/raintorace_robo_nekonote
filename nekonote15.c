/*
jiki
2015/12/4
ライントレース
曲がり角を検知するために分ける
*/
#include "kernel.h"
#include "kernel_id.h"
#include "ecrobot_interface.h"
#include "stdlib.h"

#define COLOR_SENSOR NXT_PORT_S3
#define L_MOTOR NXT_PORT_B
#define R_MOTOR NXT_PORT_C
#define L_LIGHT NXT_PORT_S1
#define R_LIGHT NXT_PORT_S2

#define BLACK_VAL 620
#define WHITE_VAL 400
#define GRAY_VAL 500
#define BASE_SPEED 40
#define P_GAIN 0.40
#define D_GAIN 10.0

DeclareCounter(SysTimerCnt);
DeclareTask(Task1);

void ecrobot_device_initialize(){
	ecrobot_init_bt_slave("LEJOS-OSEK");
	ecrobot_set_light_sensor_active(L_LIGHT);
	ecrobot_set_light_sensor_active(R_LIGHT);
	nxt_motor_set_speed(L_MOTOR,0,0);
	nxt_motor_set_speed(R_MOTOR,0,0);
	nxt_motor_set_speed(NXT_PORT_A,0,0);
	ecrobot_init_nxtcolorsensor(COLOR_SENSOR, NXT_COLORSENSOR);

}		//OSEK起動時の処理

void ecrobot_device_terminate(){
	ecrobot_set_light_sensor_inactive(L_LIGHT);
	ecrobot_set_light_sensor_inactive(R_LIGHT);
	nxt_motor_set_speed(L_MOTOR,0,0);
	nxt_motor_set_speed(R_MOTOR,0,0);
	ecrobot_term_bt_connection();
	nxt_motor_set_speed(NXT_PORT_A,0,0);
}		//OSEK終了時の処理

void user_1ms_isr_type2(void){
	StatusType ercd;

	ercd = SignalCounter(SysTimerCnt); /* Increment OSEK Alarm Counter */
	if (ercd != E_OK)
	{
		ShutdownOS(ercd);
	}
}

TASK(Task1)
{
	static int right_turn,left_turn;
	static int light,light2,light_tmp=0,light_tmp2=0;
	static int n0_light,n0_light2,n1_light,n1_light2;
	static int left_speed,right_speed;
	static int n=0,m,l;
	static int n_l_light[100],n_r_light[100];
	
	static int state=0;
	static int mode=0;
	static int rotate_angle,arm_angle;	
	
	static float x,online,y;
	static S8 count=0;
	static S8 i,j;
	
	//カラーセンサー用
	ecrobot_process_bg_nxtcolorsensor();
	ecrobot_set_nxtcolorsensor(COLOR_SENSOR,NXT_COLORSENSOR);//動作モード設定
	S16 rgb[3];
	ecrobot_get_nxtcolorsensor_rgb(COLOR_SENSOR, rgb);
	
	display_goto_xy(0, 2);
	display_string("R:");
	display_int(rgb[0],0);
	display_goto_xy(0, 3);
	display_string("G:");
	display_int(rgb[1],0);
	display_goto_xy(0, 4);
	display_string("B:");
	display_int(rgb[2],0);
	display_update();

	
	
	n0_light = n1_light;
	n0_light2 = n1_light2;

	n1_light = ecrobot_get_light_sensor(R_LIGHT);
	n1_light2 = ecrobot_get_light_sensor(L_LIGHT);//調整とかする？
	
	
	
	light=(n0_light+n1_light)/2;
	light2=(n0_light2+n1_light2)/2;
	
	right_turn = P_GAIN * (light - GRAY_VAL)*100 / (BLACK_VAL - WHITE_VAL) 
	     + D_GAIN * (light-light_tmp)/(float)(BLACK_VAL - WHITE_VAL )*100;
		 
	left_turn = P_GAIN * (light2 - GRAY_VAL)*100 / (BLACK_VAL - WHITE_VAL) 
	     + D_GAIN * (light2-light_tmp2)/(float)(BLACK_VAL - WHITE_VAL )*100;
		 
	light_tmp=light;
	light_tmp2=light2;
	
	n_r_light[i%100]=light;
	n_l_light[i%100]=light2;
	
	
	if(state==0){
		mode=1;
		
		if(!nxt_motor_get_count(NXT_PORT_A) > 0) nxt_motor_set_speed(NXT_PORT_A,+40,1);
		
		if(light>GRAY_VAL+20){
			state=1;
			rotate_angle=nxt_motor_get_count(L_MOTOR);
		}
	}
	if(state==1){
		mode=2;
	}
	if(state==2){
		mode=0;
		//カラーセンサデータからたまをつかむ
		if((nxt_motor_get_count(L_MOTOR) > rotate_angle+300)&&(rgb[0]<250 || rgb[0]>430 || nxt_motor_get_count(L_MOTOR) > rotate_angle+340)){
			nxt_motor_set_speed(L_MOTOR,0,0);
			nxt_motor_set_speed(R_MOTOR,0,0);
			state=3;
		}
		
	}
	if(state==3){
		mode=0;
		if(nxt_motor_get_count(NXT_PORT_A) > 0) nxt_motor_set_speed(NXT_PORT_A,-40,1);
		else {
			rotate_angle=nxt_motor_get_count(L_MOTOR);
			state=4;
		}
	}
	if(state==4){
		mode=3;
	}
	if(state==5){
		mode=1;
		if(light>GRAY_VAL+20){
			state=6;
			rotate_angle=nxt_motor_get_count(L_MOTOR);
		}
	}
	if(state==6){
		mode=4;
	}
	if(state==7){
		mode=1;
		if(light>GRAY_VAL+20){
			state=8;
			rotate_angle=nxt_motor_get_count(L_MOTOR);
		}
	}
	if(state==8){
		mode=5;
	}
	if(state==9){
		mode=0;
		if(light2>GRAY_VAL+20){
			state=9;
			rotate_angle=nxt_motor_get_count(L_MOTOR);
		}
	}
	
	
	//-----------------------------------------------------------------
	if(mode==2){
		nxt_motor_set_speed(L_MOTOR,40,1);
		nxt_motor_set_speed(R_MOTOR,-40,1);
		if(nxt_motor_get_count(L_MOTOR) > rotate_angle+120){
			rotate_angle=nxt_motor_get_count(L_MOTOR);
			state=2;
		}
	}
	if(mode==3){
		nxt_motor_set_speed(L_MOTOR,40,1);
		nxt_motor_set_speed(R_MOTOR,-38,1);
		if(nxt_motor_get_count(L_MOTOR) > rotate_angle+230){
			rotate_angle=nxt_motor_get_count(L_MOTOR);
			state=5;
		}
	}
	if(mode==4){
		nxt_motor_set_speed(L_MOTOR,35,1);
		nxt_motor_set_speed(R_MOTOR,-48,1);
		if(nxt_motor_get_count(L_MOTOR) > rotate_angle+120){
			rotate_angle=nxt_motor_get_count(L_MOTOR);
			state=7;
		}
	}
	if(mode==5){
		nxt_motor_set_speed(L_MOTOR,40,1);
		nxt_motor_set_speed(R_MOTOR,-40,1);
		if(nxt_motor_get_count(L_MOTOR) > rotate_angle+120){
			rotate_angle=nxt_motor_get_count(L_MOTOR);
			state=9;
		}
	}
	
	
	//右でよむ---------------------------------------------------------------
	
	if(mode==0){
		if(n_r_light[m]>GRAY_VAL){
			if(count<100){
				count+=2;
			}else count=100;
		}else{
			if(count>-100){
				count--;
			}else count=-100;
		}
		if(online==2 && count>20){
			online=2;
		}else if(count>20){
			online=1;
		}else online=0;
	
		if(online==1){
			l++;
			if(l>80){
				online=2;
				l=0;
			}
			// display_goto_xy(0, 0);
			// display_string("on line");
			// display_int(n_r_light[i],0);	
		}else{
			// display_goto_xy(0, 0);
			// display_string("lost line");
			// display_int(n_r_light[i],0);
			l=0;	
		}
		
		display_goto_xy(0, 1);
		display_string("on=");
		display_int(online,0);
		display_goto_xy(0, 2);
		display_string("l=");
		display_int(l,0);
		display_goto_xy(0, 3);
		display_string("count=");
		display_int(count,0);
		display_goto_xy(0, 4);
		display_string("m=");
		display_int(m,0);
		display_goto_xy(0, 5);
		display_string("r_light=");
		display_int(light,0);
		display_goto_xy(0, 6);
		display_string("l_light=");
		display_int(light2,0);
		display_update();
		
	
		left_speed=BASE_SPEED+right_turn;
		right_speed=BASE_SPEED-right_turn;
		
		if(online==0||online==1){
			left_speed+=(right_turn)/2;
			right_speed-=(right_turn)/2;
		}
		
		if(online==2){
			left_speed+=50;
			right_speed+=50;
			l=0;
		}
		if(state==2){
			left_speed/=2;
			right_speed/=2;
		}
		
		nxt_motor_set_speed(L_MOTOR,left_speed,1);
		nxt_motor_set_speed(R_MOTOR,right_speed,1);
	}
	
	//-------------------------------------------------------------
	
	//左で読む---------------------------------------------------------------
	
	if(mode==1){
		if(n_l_light[m]>GRAY_VAL){
			if(count<100){
				count+=2;
			}else count=100;
		}else{
			if(count>-100){
				count--;
			}else count=-100;
		}
		if(online==2 && count>20){
			online=2;
		}else if(count>20){
			online=1;
		}else online=0;
	
		if(online==1){
			l++;
			if(l>80){
				online=2;
				l=0;
			}
			// display_goto_xy(0, 0);
			// display_string("on line");
			// display_int(n_l_light[i],0);	
		}else{
			// display_goto_xy(0, 0);
			// display_string("lost line");
			// display_int(n_l_light[i],0);
			l=0;	
		}
		
		display_goto_xy(0, 1);
		display_string("on=");
		display_int(online,0);
		display_goto_xy(0, 2);
		display_string("l=");
		display_int(l,0);
		display_goto_xy(0, 3);
		display_string("count=");
		display_int(count,0);
		display_goto_xy(0, 4);
		display_string("m=");
		display_int(m,0);
		display_goto_xy(0, 5);
		display_string("r_light=");
		display_int(light,0);
		display_goto_xy(0, 6);
		display_string("l_light=");
		display_int(light2,0);
		display_update();
		
	
		left_speed=BASE_SPEED-left_turn;
		right_speed=BASE_SPEED+left_turn;
		
		if(online==0||online==1){
			left_speed-=(left_turn)/2;
			right_speed+=(left_turn)/2;
		}
		
		if(online==2){
			left_speed+=50;
			right_speed+=50;
			l=0;
		}
		
		if(state==2){
			left_speed/=2;
			right_speed/=2;
		}
		
		nxt_motor_set_speed(L_MOTOR,left_speed,1);
		nxt_motor_set_speed(R_MOTOR,right_speed,1);
	}
	
	//-------------------------------------------------------------


	


	// display_goto_xy(0, 1);
	// display_string("i am=");
	// display_int(state,0);
	// display_goto_xy(0, 2);
	// display_string("mode=");
	// display_int(mode,0);
	
	
	
	// display_goto_xy(0, 2);
	// display_string("B=");
	// display_int(BLACK_VAL,0);
	// display_string("W=");
	// display_int(WHITE_VAL,0);
	// display_goto_xy(0, 3);
	// display_string("left=");
	// display_int(light,0);
	// display_goto_xy(0, 4);
	// display_string("right=");
	// display_int(light2,0);
	// display_goto_xy(0, 5);
	// display_string("right_turn=");
	// display_int(right_turn,0);
	
	// display_goto_xy(0, 5);
	// display_string("r_light=");
	// display_int(light,0);
	// display_goto_xy(0, 6);
	// display_string("l_light=");
	// display_int(light2,0);
	// display_update();

	 display_update();

	
	ecrobot_bt_data_logger(l, online);//ログ収集
	if(i==100)i=0;

	TerminateTask();
}
